using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Threading.Tasks;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Net.Http;
using System.Runtime.CompilerServices;
using System.Xml.Linq;
using System.Diagnostics;
using ExtensionMethods;
using System.Web.Script.Serialization;

namespace ExtensionMethods
{
    public static class JSONHelper
    {
        public static string ToJSON(this object obj)
        {
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            return serializer.Serialize(obj);
        }

        public static string ToJSON(this object obj, int recursionDepth)
        {
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            serializer.RecursionLimit = recursionDepth;
            return serializer.Serialize(obj);
        }
    }
}

namespace WebApplication1
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        //You can get a valid token at https://cloud.bry.com.br/home/usuarios/autenticado/aplicacoes
        //Insert your JWT token INSERT_VALID_ACCESS_TOKEN 
        static string INSERT_VALID_ACCESS_TOKEN = "";

        Metadados metadadosObj;

        protected void Page_Load(object sender, EventArgs e)
        {
        }

        protected void Inicializar_Click(object sender, EventArgs e)
        {
            try
            {
                //The BRy-extension populates the TextBoxDadosCertificate with the data of the selected certificate, here we get this data.
                string certificateData = this.TextBoxDadosCertificado.Text;

                if (INSERT_VALID_ACCESS_TOKEN == "")
                    throw new Exception("⚠ Please Insert a valid Access Token At Line 22");

                if (this.TextBoxTipo.Text == "" || this.TextBoxUF.Text == "")
                    throw new Exception("⚠ Please, check provived information");

                string metadados = geraJsonMetadados();

                string responseInitialize = Inicialize(certificateData, metadados).Result;

                ResponseInitialize responseInitializeObj = new ResponseInitialize();

                //This function returns the data in JSON Object as a String. We need to Deserialize to use this data as an object
                responseInitializeObj = Deserialize<ResponseInitialize>(responseInitialize);

                //If an error occurs in the Deserialization, we throw an exception
                if (responseInitializeObj.assinaturasInicializadas == null)
                    throw new Exception(responseInitialize + "\\n⚠ Please, check if you have credits available and entered a valid JWT token.");

                //We populate the TextBoxSaidaInitializar with the data returned from the FW-HUB
                this.TextBoxSaidaInicializar.Text = responseInitialize;

                //We prepared an object that will be serialized and used to populate the TextBoxEntradaExtensao
                InputExtension inputExtension = new InputExtension
                {
                    algoritmoHash = responseInitializeObj.algoritmoHash,
                    nonce = responseInitializeObj.nonce,
                    formatoDadosEntrada = responseInitializeObj.formatoDadosEntrada,
                    formatoDadosSaida = responseInitializeObj.formatoDadosSaida
                };

                for (int i = 0; i < responseInitializeObj.assinaturasInicializadas.Count; ++i)
                {
                    var input = new ExtensionAssinaturas();
                    input.nonce = responseInitializeObj.assinaturasInicializadas[i].nonce;
                    input.hashes.Add(responseInitializeObj.assinaturasInicializadas[i].messageDigest);
                    inputExtension.assinaturas.Add(input);
                }
                this.TextBoxEntradaExtensao.Text = Serialize<InputExtension>(inputExtension);
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "alerts", "javascript:alert('" + ex.Message + "')", true);
            }
        }

        protected void Finalize_Click(object sender, EventArgs e)
        {
            try
            {
                //We get the data returned from the fw-hub
                String responseInitialize = this.TextBoxSaidaInicializar.Text;

                ResponseInitialize responseInitializeObj = new ResponseInitialize();

                //This function returns the data in JSON Object as a String. We need to Deserialize to use this data as an object
                responseInitializeObj = Deserialize<ResponseInitialize>(responseInitialize);

                //Object generated from the BRy-extension's output
                OutputExtension outputExtension = Deserialize<OutputExtension>(this.TextBoxSaidaExtensao.Text);

                //We call the Finalize method extension output, the initialization return and the certificate data to finalize the signature
                string responseFinalize = Finalize(outputExtension, responseInitializeObj).Result;
                //this.TextBoxFinalizar.Text = Finalize(outputExtension, responseInitializeObj).Result;
                this.TextBoxFinalizar.Text = responseFinalize;
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "alerts", "javascript:alert('" + ex.Message + "')", true);
            }
        }


        public async static Task<string> Inicialize(string certificateData, string metadados)
        {
            InicializeDataObj inicializeDataObj = new InicializeDataObj
            {
                perfil = "CARIMBO",
                algoritmoHash = "SHA256",
                formatoDadosEntrada = "Base64",
                formatoDadosSaida = "Base64",
                certificado = certificateData
            };

            //Buscar o caminho da aplicação
            string path = AppDomain.CurrentDomain.BaseDirectory;

            //Lendo o documento do disco e convertendo para Streamcontent
            var fileStream = new FileStream(path + "./documento.pdf", FileMode.Open);
            var streamContentDocument = new StreamContent(fileStream);

            //configurando a imagem 
            var imageStream = new FileStream(path + "./imagem.jpg", FileMode.Open);
            var streamContentImage = new StreamContent(imageStream);

            ConfiguracaoImagem configuracao_imagem = new ConfiguracaoImagem();

            configuracao_imagem.altura = 30;
            configuracao_imagem.largura = 100;
            configuracao_imagem.posicao = "INFERIOR_ESQUERDO";
            configuracao_imagem.pagina = "PRIMEIRA";
            configuracao_imagem.proporcaoImagem = 30;

            string configuracao_imagemSerialized = Serialize(configuracao_imagem);

            ConfiguracaoTexto configuracao_texto = new ConfiguracaoTexto();

            configuracao_texto.texto = "Personalização";
            configuracao_texto.incluirCN = "true";
            configuracao_texto.incluirCPF = "true";
            configuracao_texto.incluirEmail = "true";

            string configuracao_textoSerialized = Serialize(configuracao_texto);

            //Aqui montamos a requisição com os dados no formado Multipart
            var requestContent = new MultipartFormDataContent();

            //É possível adicionar vários documentos aqui, para isto cada documento deve possuir um nonce      
            inicializeDataObj.nonces.Add("123");
            requestContent.Add(streamContentDocument, "documento", "documento.pdf");
            // fim loop

            //Serializando o Objeto
            string dados_inicializarSerialized = Serialize(inicializeDataObj);

            requestContent.Add(new StringContent(dados_inicializarSerialized), "dados_inicializar");
            requestContent.Add(new StringContent(configuracao_imagemSerialized), "configuracao_imagem");
            requestContent.Add(new StringContent(configuracao_textoSerialized), "configuracao_texto");
            requestContent.Add(new StringContent(metadados), "metadados");
            requestContent.Add(streamContentImage, "imagem", "imagem.jpg");
            HttpClient client = new HttpClient();
           
            //client.DefaultRequestHeaders.Add("fw_credencial", credencial);
            client.DefaultRequestHeaders.Add("Authorization", INSERT_VALID_ACCESS_TOKEN);

            var response = await client.PostAsync("https://hub2.bry.com.br/fw/v1/pdf/pkcs1/assinaturas/acoes/inicializar", requestContent).ConfigureAwait(false);

            // O resultado do BRy Framework é um json
            string result = await response.Content.ReadAsStringAsync().ConfigureAwait(false);

            return result;
        }

        public async static Task<string> Finalize(OutputExtension outputExtension, ResponseInitialize responseInitializeObj)
        {
            //Criamos o Objeto para finalizar
            FinalizeData finalizeData = new FinalizeData();

            //Percorremos os dados cifrados pela extensão e os adicionamos ao objeto de finalização
            for (int i = 0; i < outputExtension.assinaturas.Count; ++i)
            {
                FinalizeDataInternal finalizeDataInt = new FinalizeDataInternal();
                finalizeDataInt.nonce = outputExtension.assinaturas[i].nonce;
                finalizeDataInt.cifrado = outputExtension.assinaturas[i].hashes[0];
                finalizeData.assinaturasPkcs1.Add(finalizeDataInt);
            }

            finalizeData.nonce = responseInitializeObj.nonce;
            finalizeData.formatoDeDados = responseInitializeObj.formatoDadosSaida;

            string FinalizarSerialized = Serialize<FinalizeData>(finalizeData);

            HttpClient client = new HttpClient();
            //Autenticação
            client.DefaultRequestHeaders.Add("Authorization", INSERT_VALID_ACCESS_TOKEN); 

            var response = await client.PostAsync("https://hub2.bry.com.br/fw/v1/pdf/pkcs1/assinaturas/acoes/finalizar", new StringContent(FinalizarSerialized, Encoding.UTF8, "application/json")).ConfigureAwait(false);

            string result = await response.Content.ReadAsStringAsync().ConfigureAwait(false);
            return result;
        }
        public static string Serialize<T>(T obj)
        {
            DataContractJsonSerializer serializer = new DataContractJsonSerializer(obj.GetType());
            MemoryStream ms = new MemoryStream();
            serializer.WriteObject(ms, obj);
            string retVal = Encoding.UTF8.GetString(ms.ToArray());
            return retVal;
        }
        public static T Deserialize<T>(string json)
        {
            T obj = Activator.CreateInstance<T>();
            MemoryStream ms = new MemoryStream(Encoding.Unicode.GetBytes(json));
            DataContractJsonSerializer serializer = new DataContractJsonSerializer(obj.GetType());
            obj = (T)serializer.ReadObject(ms);
            ms.Close();
            return obj;
        }
    
        public string geraJsonMetadados()
        {
            string metadadosJs;
            this.metadadosObj = new Metadados
            {
                tipoDocumento = this.DropDownTipoDoc.Text,
                tipoMedico = this.DropDownTipoProfissional.Text,
                numero = this.TextBoxTipo.Text,
                uf = this.TextBoxUF.Text,
                especialidade = this.TextBoxEspecialidade.Text
            };
            //Inserindo OIDs respectivos ao tipo de profissinal (caso Médico, insere os OIDs do farmacêutico com valores nulos)
            if (metadadosObj.tipoMedico == "Medico")
            {
                metadadosObj.numeroOID = "2.16.76.1.4.2.2.1";
                metadadosObj.ufOID = "2.16.76.1.4.2.2.2";
                metadadosObj.especialidadeOID = "2.16.76.1.4.2.2.3";
                metadadosJs = "{\"" + metadadosObj.tipoDocumento + "\"" + ":" + "\"" + "\"" + "," + "\"" + metadadosObj.numeroOID + "\":\"" + metadadosObj.numero + "\",\"" + metadadosObj.ufOID + "\":\"" + metadadosObj.uf + "\",\"" + metadadosObj.especialidadeOID + "\":\"" + metadadosObj.especialidade + "\",\"" + "2.16.76.1.4.2.3.1" + "\":\"" + "" + "\"," + "\"" + "2.16.76.1.4.2.3.2" + "\":\"" + "" + "\"," + "\"" + "2.16.76.1.4.2.3.3" + "\":\"" + "" + "\"" + "}";
            }
            else if (metadadosObj.tipoMedico == "Farmaceutico")
            {
                metadadosObj.numeroOID = "2.16.76.1.4.2.3.1";
                metadadosObj.ufOID = "2.16.76.1.4.2.3.2";
                metadadosObj.especialidadeOID = "2.16.76.1.4.2.3.3";
                metadadosJs = "{\"" + metadadosObj.tipoDocumento + "\"" + ":" + "\"" + "\"" + "," + "\"" + metadadosObj.numeroOID + "\":\"" + metadadosObj.numero + "\",\"" + metadadosObj.ufOID + "\":\"" + metadadosObj.uf + "\",\"" + metadadosObj.especialidadeOID + "\":\"" + metadadosObj.especialidade + "\"}";
            }
            else
            {
                metadadosObj.numeroOID = "2.16.76.1.4.2.12.1";
                metadadosObj.ufOID = "2.16.76.1.4.2.12.2";
                metadadosObj.especialidadeOID = "2.16.76.1.4.2.12.3";
                metadadosJs = "{\"" + metadadosObj.tipoDocumento + "\"" + ":" + "\"" + "\"" + "," + "\"" + metadadosObj.numeroOID + "\":\"" + metadadosObj.numero + "\",\"" + metadadosObj.ufOID + "\":\"" + metadadosObj.uf + "\",\"" + metadadosObj.especialidadeOID + "\":\"" + metadadosObj.especialidade + "\"}";
            }
            return metadadosJs;
        }
    }

    // Data Contracts, utilizados para Serializar e Desserializar JSON Objects

    [DataContract]
    public class InicializeDataObj
    {
        [DataMember]
        public string perfil;

        [DataMember]
        public string algoritmoHash;

        [DataMember]
        public string formatoDadosEntrada;

        [DataMember]
        public string formatoDadosSaida;

        [DataMember]
        public string certificado;

        [DataMember]
        public List<string> nonces;

        public InicializeDataObj()
        {
            this.nonces = new List<string>();
        }
    }

    [DataContract]
    public class ConfiguracaoImagem
    {
        [DataMember]
        public int altura;

        [DataMember]
        public int largura;

        [DataMember]
        public string posicao;

        [DataMember]
        public string pagina;

        [DataMember]
        public int proporcaoImagem;
    }


    [DataContract]
    public class ConfiguracaoTexto
    {
        [DataMember]
        public string texto;

        [DataMember]
        public string incluirCN;

        [DataMember]
        public string incluirCPF;

        [DataMember]
        public string incluirEmail;

        [DataMember]
        public string rotacaoTexto;
    }

    [DataContract]
    public class ResponseInitialize
    {
        [DataMember]
        public string nonce;

        [DataMember]
        public string formatoDadosEntrada;

        [DataMember]
        public string formatoDadosSaida;

        [DataMember]
        public string algoritmoHash;

        [DataMember]
        public List<AssinaturasInicializadasIntern> assinaturasInicializadas;
    }

    [DataContract]
    public class AssinaturasInicializadasIntern
    {
        [DataMember]
        public string messageDigest;

        [DataMember]
        public string nonce;
    }

    [DataContract]
    public class InputExtension
    {
        [DataMember]
        public string algoritmoHash;

        [DataMember]
        public string nonce;

        [DataMember]
        public string formatoDadosEntrada;

        [DataMember]
        public string formatoDadosSaida;

        [DataMember]
        public List<ExtensionAssinaturas> assinaturas;

        public InputExtension()
        {
            this.assinaturas = new List<ExtensionAssinaturas>();
        }
    }

    [DataContract]
    public class ExtensionAssinaturas
    {
        [DataMember]
        public string nonce;

        [DataMember]
        public List<string> hashes;

        public ExtensionAssinaturas()
        {
            this.hashes = new List<string>();
        }
    }

    [DataContract]
    public class OutputExtension
    {
        [DataMember]
        public string nonce;

        [DataMember]
        public List<ExtensionAssinaturas> assinaturas;

        public OutputExtension()
        {
            this.assinaturas = new List<ExtensionAssinaturas>();
        }
    }

    [DataContract]
    public class FinalizeDataInternal
    {
        [DataMember]
        public string cifrado;

        [DataMember]
        public string nonce;
    }

    public class FinalizeData
    {
        [DataMember]
        public string nonce;

        [DataMember]
        public string formatoDeDados;

        [DataMember]
        public List<FinalizeDataInternal> assinaturasPkcs1;

        public FinalizeData()
        {
            this.assinaturasPkcs1 = new List<FinalizeDataInternal>();
        }
    }

    [DataContract]
    public class Metadados
    {
        [DataMember]
        public string tipoDocumento;

        [DataMember]
        public string tipoMedico;

        [DataMember]
        public string numero;

        [DataMember]
        public string numeroOID;

        [DataMember]
        public string uf;

        [DataMember]
        public string ufOID;

        [DataMember]
        public string especialidade;

        [DataMember]
        public string especialidadeOID;
    }
}

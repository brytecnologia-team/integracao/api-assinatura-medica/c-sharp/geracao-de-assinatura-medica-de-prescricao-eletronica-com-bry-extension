# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]
 
Empty

## [1.1.0] - 2020-04-27

### Added
- Image and Text configuration options.

### Fixed
- Error messages

## [1.0.0] - 2020-03-11

### Added

- A SIGNATURE generation example. 

[1.0.0]:https://gitlab.com/brytecnologia-team/integracao/api-assinatura/c-sharp/assinatura-pdf-com-bry-extension/-/tags/1.0.0
[1.1.0]:https://gitlab.com/brytecnologia-team/integracao/api-assinatura/c-sharp/assinatura-pdf-com-bry-extension/-/tags/1.1.0